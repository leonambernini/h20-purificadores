﻿const loader = '<i class="fas fa-sync fa-spin"></i>',
    prevArrow = `<button type="button" class="slick-prev btn"><i class="fas fa-angle-left"></i></button>`,
    nextArrow = `<button type="button" class="slick-next btn"><i class="fas fa-angle-right"></i></button>`;


const variables = {
    loader: loader,
    headers: {
        "Accept": "application/vnd.vtex.ds.v10+json",
        "Content-Type": "application/json",
        "x-vtex-api-appKey": "vtexappkey-mercostore-BRXSAS",
        "x-vtex-api-appToken": "TKGTYIZOAWPJDZCGUBPIKWXPAWUGRLCRRAPFYABNLCFQNOWRKXUQVKBXNRSBOTWOTXRCJXIUACWIQGBMZCWUQMVHYSRHIZZSFQWPWTHMOAPTNTGGPGCLWCYHCKERFBLB"
    },
    carousel: {
        dots: true,
        arrows: true,
        infinite: true,
        prevArrow: prevArrow,
        nextArrow: nextArrow
    },
    cart: {
        // structs
        elementMain: 'admake-cart-sidebar',
        title: 'Seu Carrinho',
        btnClose: '<i class="fas fa-times"></i>',
        loader: loader,
        selectorBadge: '.col-header-cart .badge',
        btnBuyMore: '',
        btnGoCart: 'Finalizar Carrinho',
        subTotal: true,
        total: false,
        discount: false,
        // alets: [],
        alerts: ['* Frete calculado na finalização do pedido.'],

        // items
        qtyItem: true,
        listPrice: false,
        bestPrice: true,
        btnRemove: '<i class="fas fa-trash"></i>',

        // buy
        buyAsyn: true,
        elementBuy: '.buy-button-normal > a, #ad-product-buy-button .buy-button',
        elementQty: '.btn-add-remove'
    },
    showcaseDepartment: {
        // scroll
        infiniteScroll: false,
        btnLoadMore: true,
        titleBtnLoad: 'Carregar mais produtos',
        showcase: '.admake-showcase[id*=ResultItems]',
        loader: loader,

        // filter
        orderButtons: false,
        classesOrder: 'd-none d-md-block flex-wrap d-md-flex justify-content-between align-items-center text-center'
    },
    productImage: {
        carouselDesktop: false,
        mobile: true,
        thumbsQty: 5,
        thumbVertical: false,
        prevArrow: prevArrow,
        nextArrow: nextArrow,
    },
    shipping: {
        loader: loader,
        calculateQty: true,
        titleValue: 'Valores',
        titleMsg: 'Opções de Entrega',
        resultInfo: 'Frete {S-NAME}, entrega em {S-DAYS} para o CEP {S-CEP}',
        btnCalc: '#btn-shipping',
        inputText: '#cep-shipping'
    },
    reviews: {
        column: false,
        pagination: false,
        qtyByPage: 1
    },
    navbar: {
        // struct
        version: 'manual', // (auto, manual)
        levels: 2, // ( > 2 somente na versao auto )
        fluid: false,
        subContainer: true,
        qtyNivel3: 5,
        showMore: 'ver mais',

        // all department - qtyMax (auto), orientation (vertical, horizontal)
        allDepartment: true,
        titleAll: "Todos os <br> Departamentos",
        qtyMax: 0,

        mobile: true,
        mobileElement: '#header-nav-mobile .nav-menu ul',
        mobileBtnElement: '#header-bar-menu',
        mobileType: 'dropdown'
    },
    discountProgressive: {
        idCalculator: '718ad4e6-e2b1-4456-8775-6e457b7db377',
        textQty: 'Cantitate',
        textDiscount: 'Discount',
        btnBuy: '<i class="fas fa-cart-plus"></i>'
    },
    estimateDate: {
        title: 'LA CERERE',
        text: 'Produs disponibil la cerere. Livrarea produsului se va face in cel mult 30 de zile de la plasarea comenzii.'
    }
}

export default function getVariables(param) {
    return (param !== undefined && param !== null && variables[param] !== undefined) ? variables[param] : variables;
}