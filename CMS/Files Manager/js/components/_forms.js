﻿import { serializeArrayToJson } from './_functions.js'

export default class Forms {
    constructor(element = '[data-admake="masterdata-forms"]') {
        this._element = element;
        this.createForm();
        this.createMask();
        this.events();
    }

    resetForm($form) {
        $.each($form.find('.form-control'), function () {
            $(this).val('');
        });
    }

    checkForm(store, sigla, $form, $btnSubmit) {
        if (store == undefined || store == null || store == '') {
            return false;
        } else if (sigla == undefined || sigla == null || sigla == '') {
            return false;
        } else if (!$form.length) {
            return false;
        } else if (!$btnSubmit.length) {
            return false;
        }
        return true
    }

    createMask() {
        if ($.fn.mask) {
            $(".input-cnpj").mask('00.000.000/0000-00');
            $(".input-cpf").mask('000.000.000-00');
            $(".input-cep").mask('00000-000');
            $(".input-phone").mask('(00) 0000-00009');
            $(".input-phone").blur(function (event) {
                if ($(this).val().length == 15) { // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
                    $(this).mask('(00) 00000-0009');
                } else {
                    $(this).mask('(00) 0000-00009');
                }
            });
        }
    }

    createForm() {
        const self = this;
        $.each($(this._element), function () {
            const $this = $(this);

            let store = $this.attr('data-store'),
                sigla = $this.attr('data-sigla'),
                $form = $this.find($this.attr('data-selector')),
                $btnSubmit = $form.find('button[type="submit"]'),
                $fields = $this.find('.form-field'),
                url = "/api/dataentities/" + sigla + "/documents/";

            if (self.checkForm(store, sigla, $form, $btnSubmit)) {
                $form.attr('action', url);

                $.each($fields, function () {
                    const $t = $(this);

                    let htmlField = '',
                        name = $t.attr('name') || '',
                        label = $t.attr('label') || '',
                        mlength = $t.attr('max-length') || '0',
                        type = $t.attr('type') || 'text',
                        required = $t.attr('required') || 'false',
                        placeholder = $t.attr('placeholder') || '';

                    htmlField += '<div class="form-group form-group-' + sigla + '-' + name + '">';

                    if (label !== '') {
                        htmlField += '	<label for="' + sigla + '-' + name + '">' + label + '</label>';
                    }

                    if (type == 'select') {

                    } else {
                        htmlField += '<div class="input-skew"><input type="' + type + '" autocomplete="off" placeholder="' + placeholder + '" name="' + name + '" id="' + sigla + '-' + name + '" ' + ((mlength > 0) ? 'max-length="' + mlength + '"' : '') + ' class="form-control input-' + type + '" ' + ((required) ? 'required="required"' : '') + '"><span class="input-focus"></span></div>';
                    }

                    htmlField += '</div>';
                    $t.html(htmlField);
                });
            }
        });
    }

    events() {
        const $el = $(this._element), self = this;
        let $form = $el.find($el.attr('data-selector'));

        $form.submit(function () {
            let $this = $(this),
                $btnSubmit = $form.find('button[type="submit"]'),
                textMain = $btnSubmit.text(),
                btnLoader = $btnSubmit.attr('loader'),
                btnSent = $btnSubmit.attr('success');

            $.ajax({
                headers: {
                    "Accept": "application/vnd.vtex.ds.v10+json",
                    "Content-Type": "application/json"
                },
                data: JSON.stringify(serializeArrayToJson($this.serializeArray())),
                type: 'PATCH',
                url: $this.attr('action'),
                beforeSend: function () {
                    $btnSubmit.html(btnLoader);
                },
                success: function (data) {
                    $btnSubmit.html(btnSent);
                    self.resetForm($this);
                },
                error: function (data) {
                    if ($this.find('.alert').length) {
                        $this.find('.alert').html('Ocorreu um erro, tente novamente.');
                    } else {
                        $this.append('<p class="alert alert-danger">Ocorreu um erro, tente novamente.</p>');
                    }
                    $btnSubmit.html(textMain);
                }
            });

            return false;
        });
    }

}
