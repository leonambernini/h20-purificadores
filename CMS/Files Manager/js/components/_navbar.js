import getVariables from './_variables.js'
import { stringToSlug } from './_functions'

const _variables = getVariables();

export default class Menu {
    constructor(element = '[data-admake="navbar"]') {
        if( $('#header-nav').length ){
            $('.open-close-nav-mobile, #header-nav .mask').click( function(){
                $('#header-nav').toggleClass('mobile-active');
                return false;
            });

            $('.header-nav-link').click( function(){
                if( $(window).width() < 768 ){
                    let $this = $(this);
                    let $parent = $this.parents('.header-nav-item');
                    if( $('+ .header-nav-sub-box', $this).length && !$parent.hasClass('active') ){
                        $parent.addClass('active');
                        return false;
                    }
                }
            });
        }
    }
    createStruct(data) {
        let html = '';

        data.forEach((lvl1, index) => {
            let srcBanner = $('.navbar-banners[data-index="' + index + '"] img').attr('src');

            if (lvl1.children) {
                html += `<li class="lvl-1-item has-children ${stringToSlug(lvl1.name)}">
                            <a href="${lvl1.url}" class="link-lvl-1">
                                <span class="icon"><img src="/arquivos/navbar-${stringToSlug(lvl1.name)}.png" alt="icone para o menu ${lvl1.name}"/></span>
                                <span class="text">${lvl1.name}</span>
                            </a>
                            <div class="nav-sub lvl-2">
                                <ul>`;

                lvl1.children.forEach(lvl2 => {
                    html += `<li class="lvl-2-item">
                                <a href="${lvl2.url}" class="link-lvl-2">${lvl2.name}</a>
                            </li>`;
                });

                html += `</ul>`;
                html += `<div class="banner align-self-end d-none d-md-block">
                            <img src="${srcBanner}" alt="banner da categoria ${lvl1.name}"/>
                        </div></div>`;
            } else {
                html += `<li class="lvl-1-item ${stringToSlug(lvl1.name)}">
                            <a href="${lvl1.url}" class="link-lvl-1">
                            <span class="icon"><img src="/arquivos/navbar-${stringToSlug(lvl1.name)}.png" alt="icone para o menu ${lvl1.name}"/></span>
                            <span class="text">${lvl1.name}</span>
                        </a>`;
            }

            html += `</li>`;
        });

        if (html != '') {
            if (this._config.allDepartment) {
                html = `<li style="display:none;" class="all-departments lvl-1-item">
                                <span class="link-all-departments">${this._config.titleAll}</span>             
                                <ul class="nav-sub lvl-1">${html}</ul>
                            </li>` + html;
            }

            html = `<ul class="nav-items navbar">${html}</ul>`;
            this._$el.append(html);
        }

        if (this._config.mobile) {
            $(this._config.mobileElement).append(html);
            if (this._config.mobileType == 'dropdown') {
                $(this._config.mobileElement).find('.navbar').removeClass('navbar');
                $(this._config.mobileElement).find('.has-children').append(`<button class="btn-dropdown btn"><i class="fas fa-caret-down"></i></button>`);
            }
        }

    }
    ajustNav() {
        const $nav = this._$el.find('.nav-items');

        if ($(window).width() > 767) {
            const separatorClass = 'no-separator';
            const width = $nav.width();
            const $allDepartments = $nav.find('.all-departments');
            const $items = $nav.find('>li');

            let lastItem = 0;
            let allDepartmentsWidth = 0;
            let haveAllDepartments = false;
            let isLarge = false;
            let liWidth = 0;
            let count = 0;

            if ($allDepartments.length) {
                allDepartmentsWidth = $allDepartments.outerWidth();
                $allDepartments.addClass(separatorClass).hide();
                haveAllDepartments = true;
            }

            $.each($items, function () {
                let $this = $(this);
                liWidth += $this.outerWidth();

                if (liWidth - allDepartmentsWidth > width) {
                    $this.addClass(separatorClass).hide();
                    isLarge = true;

                    if (count++ == 0) {
                        let temp = lastItem;
                        let tempLiWidth = liWidth;
                        while (tempLiWidth > width) {
                            tempLiWidth -= $items.eq(temp).outerWidth();
                            $items.eq(temp).addClass(separatorClass).hide();
                            temp--;
                        }
                        allDepartmentsWidth = 0;
                    }
                } else if (!$this.hasClass('all-departments')) {
                    $this.removeClass(separatorClass).show();
                }
                lastItem++;
            });

            if (haveAllDepartments && isLarge) {
                $nav.find('.all-departments').removeClass(separatorClass).show();
            }
        } else {
            $('>li', $nav).not('.all-departments').show();
        }
    }
    controller() {
        if (this._config.version == 'auto') {
            $.ajax({
                "url": `/api/catalog_system/pub/category/tree/${(this._config.levels - 1)}/`,
                "method": "GET",
                "headers": {
                    "Content-Type": "application/json",
                },
                success: (data) => {
                    this.createStruct(data);
                    this.ajustNav();
                },
                error: function (data, status) {
                    console.log('errors' + status);
                }
            });
        } else if (this._config.version == 'manual') {
            let $menuVtex = $('#menu-vtex .menu-departamento'), data = [];

            if ($menuVtex.length) {
                $menuVtex.find('> h3').each(function () {
                    let $list = $(this).find('+ ul li');

                    if ($list.length) {
                        let dataChildren = [];
                        $list.each(function () {
                            dataChildren.push({
                                name: $(this).find('>a').text(),
                                url: $(this).find('> a').attr('href')
                            })
                        });

                        data.push({
                            name: $(this).find('> a').text(),
                            url: $(this).find('> a').attr('href'),
                            children: dataChildren
                        });
                    } else {
                        data.push({
                            name: $(this).find('> a').text(),
                            url: $(this).find('> a').attr('href')
                        });
                    }
                });

                this.createStruct(data);
                this.ajustNav();
            }
        }
    }
    events() {
        $(window).resize(() => {
            this.ajustNav();
        });
        $(document).on('click', this._config.mobileElement + ' .btn-dropdown', function () {
            $(this).closest('li, div').toggleClass('active');
        })
        $(document).on('click', '.nav-mobile-close', function () {
            $(`#header-nav-mobile > .content`).animate({
                left: '-100%'
            }, 300, function () {
                $(`#header-nav-mobile`).fadeOut(100);
            });
        });
        $(document).on('click', this._config.mobileBtnElement, function () {
            $(`#header-nav-mobile`).fadeIn(100, function () {
                $(`#header-nav-mobile > .content`).animate({
                    left: 0
                }, 300);
            });
        });
    }
}
