import getVariables from './_variables.js'
import { formatMoneyCheckout, getParameter } from './_functions.js'

const _variables = getVariables();

export default class Cart {
    constructor(parent = 'body', element = '[data-admake="cart-sidebar"]') {
        this._element = element;
        this._$el = $(element);
        this._config = null;
        this._$parent = $(parent);

        if (this._$el.length) {
            let dataConfig = this._$el.attr('data-config');

            if (dataConfig !== undefined && dataConfig !== null && dataConfig !== '') {
                dataConfig = dataConfig.replace(/\'/g, '"');
                this._config = { ..._variables.cart, ...JSON.parse(dataConfig) };
            } else {
                this._config = { ..._variables.cart };
            }

            this.createStruct();
            this.createListItems();
            this.events();
        }
    }
    createStruct() {
        const current = this._config;

        let htmlMain = `<div id="${current.elementMain}" style="display: none;">
                            <div class="mask"></div>
                            <div class="content">
                                <div class="cart-header">
                                    <strong>${current.title}</strong>
                                    <button class="btn cart-btn-close">
                                        ${current.btnClose}
                                    </button>
                                </div>
                                <div class="cart-body">
                                    <p class="cart-loader">
                                        ${current.loader}
                                        <span class="sr-only">Loading...</span>
                                    </p>
                                    <div class="cart-items"></div>
                                </div>
                                <div class="cart-footer">`;

        if (current.alerts) {
            htmlMain += '          <ul class="cart-alerts list-unstyled">';
            current.alerts.forEach(element => {
                htmlMain += '           <li>' + element + '</li>';
            });
            htmlMain += '         </ul>';
        }

        htmlMain += '             <div class="cart-values">';

        if (current.subTotal) {
            htmlMain += `           <div class="cart-subtotal d-flex justify-content-between align-items-center" style="display: none;">
                                        <span>Subtotal</span>
                                        <strong>-</strong>
                                    </div>`;
        }

        if (current.discount) {
            htmlMain += `           <div class="cart-discont d-flex justify-content-between align-items-center" style="display: none;">
                                        <span>Descontos</span>
                                        <strong>-</strong>
                                    </div>`
        }

        if (current.total) {
            htmlMain += `           <div class="cart-total d-flex justify-content-between align-items-center">
                                        <span>Total</span>
                                        <strong>-</strong>
                                    </div>`;
        }

        htmlMain += '           </div>';

        htmlMain += '		        <div class="cart-actions">';
        htmlMain += '				    <div>';

        if (current.btnBuyMore !== null && current.btnBuyMore !== '') {
            htmlMain += '				<a class="btn cart-btn-buymore cart-btn-close">' + current.btnBuyMore + '</a>';
        }
        if (current.btnGoCart !== null && current.btnGoCart !== '') {
            htmlMain += '				<a class="btn cart-btn-buy" href="/checkout/#/cart">' + current.btnGoCart + '</a>';
        }

        htmlMain += `                   </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;

        this._$parent.append(htmlMain);
    }
    createListItems(flag = null) {
        const current = this._config, $cart = $(`#${current.elementMain}`);
        let htmlItems = '';

        $cart.find('.cart-loader').show();

        vtexjs.checkout.getOrderForm().done((orderForm) => {

            if (orderForm.items.length) {
                orderForm.items.forEach((element, index) => {
                    htmlItems += `<div class="item d-flex align-items-center justify-content-between" data-sku="${element.id}" data-ref="${element.refId}" data-prod="${element.productId}">
                                        <div class="item-image">
                                            <a href="${element.detailUrl}" title="${element.name}">
                                                <img class="img-fluid" src="${element.imageUrl}"/>
                                            </a>
                                        </div>
                                        <div class="item-details">
                                            <div class="item-name">
                                                <a href="${element.detailUrl}" title="${element.name}">${element.name}</a>
                                            </div>
                                            <div class="item-price-actions">`;
                    if (current.qtyItem) {
                        htmlItems += `<span class="item-qty">${element.quantity}x</span>`;
                    }
                    if (current.listPrice) {
                        htmlItems += `<span class="item-listprice">R$ ${formatMoneyCheckout(element.listPrice, 2, ',', '.')}</span>`;
                    }
                    htmlItems += `  <span class="item-bestprice">R$ ${formatMoneyCheckout(element.price, 2, ',', '.')}</span>`;

                    if(element.sellingPrice <= 0){
                        htmlItems += `<span class="item-freeprice">(Gratuito)</span>`;
                    }
                    htmlItems += `</div></div>
                        <button class="item-remove btn" index="${index}">
                            ${current.btnRemove}
                        </button>
                    </div>`;

                });
            } else {
                htmlItems += `<button class="cart-btn-close">Carrinho vazio</button>`;
            }

            if (current.selectorBadge !== null && $(current.selectorBadge).length) {
                $(current.selectorBadge).html(orderForm.items.length);
            }

            if (current.subTotal && orderForm.value !== 0) {
                if (orderForm.totalizers[0]) {
                    $cart.find('.cart-subtotal > strong').html('R$ ' + formatMoneyCheckout(orderForm.totalizers[0].value, 2, ',', '.'));
                    $cart.find('.cart-subtotal').show();
                } else {
                    $cart.find('.cart-subtotal').hide();
                }
            }

            if (current.discount && orderForm.value !== 0) {
                if (orderForm.totalizers[1]) {
                    $cart.find('.cart-discount > strong').html('R$ ' + formatMoneyCheckout(orderForm.totalizers[1].value, 2, ',', '.'));
                    $cart.find('.cart-discount').show();
                } else {
                    $cart.find('.cart-discount').hide();
                }
            }

            $cart.find('.cart-total > strong').html('R$ ' + formatMoneyCheckout(orderForm.value, 2, ',', '.'));
            $cart.find('.cart-items').html(htmlItems);
            $cart.find('.cart-loader').hide();

            if (flag == 'open') {
                this.cartOpen();
            }
        });
    }
    removeItem(index) {
        const self = this;

        vtexjs.checkout.getOrderForm().then(function (orderForm) {
            let item = orderForm.items[index];

            item.index = index;
            return vtexjs.checkout.removeItems([item]);

        }).done(function (orderForm) {
            self.createListItems();
        });
    }
    cartClose() {
        const current = this._config;

        $(`#${current.elementMain} .content`).animate({
            right: '-100%'
        }, 300, function () {
            $(`#${current.elementMain}`).fadeOut(100);
        });
    }
    cartOpen() {
        const current = this._config;

        $(`#${current.elementMain}`).fadeIn(100, function () {
            $(`#${current.elementMain} .content`).animate({
                right: 0
            }, 300);
        });

        $('.cart-body').css('max-height', `calc(100% - ${$('.cart-footer').outerHeight() + 60}px`);

    }
    events() {
        const current = this._config,
            self = this;

        $(document).on('click', `#${current.elementMain} .mask, #${current.elementMain} .cart-btn-close`, function (e) {
            e.preventDefault();
            self.cartClose();
        });

        $(document).on('click', this._element, function (e) {
            e.preventDefault();
            self.cartOpen();
        });

        $(document).on('click', '.item-remove', function (e) {
            e.preventDefault();
            self.removeItem($(this).attr('index'));
        });

        if (current.buyAsyn) {
            $(document).on('click', current.elementBuy, function (e) {
                const $this = $(this);
                let urlButton = $this.attr('href'),
                    htmlDefault = $this.html(),

                    inputQty = $(current.elementQty).attr('element'),
                    parentQty = $(current.elementQty).attr('parent'),

                    $parents = $this.parents(parentQty),
                    qty = parseInt($parents.find(inputQty).val());

                if (urlButton.indexOf('cart') >= 0) {
                    e.preventDefault();
                    
                    if (qty < 1 || !qty) {
                        qty = 1;
                    }

                    urlButton = urlButton.replace(`qty=${getParameter('qty', urlButton)}`, `qty=${qty}`);
                    $this.attr('href', urlButton);

                    $.ajax({
                        url: urlButton,
                        beforeSend: function () {
                            $this.html(current.loader);
                        },
                        success: function () {
                            self.createListItems('open');
                        },
                        complete: () => {
                            $this.html(htmlDefault);
                        },
                        error: function () {
                            console.log('error');
                        }
                    });
                }
            });
        }

        // discountProgressive
        $(document).on('click', '.btn-discount-progressive', (e) => {
            let $btnCurrent = $(e.currentTarget), url = $btnCurrent.attr('href'),
                htmlDefault = $('.btn-discount-progressive').html();

            e.preventDefault();
            $.ajax({
                url: url,
                beforeSend: () => {
                    $btnCurrent.html(this._config.loader);
                },
                success: () => {
                    this.createListItems('open');
                },
                complete: function () {
                    $btnCurrent.html(htmlDefault);
                },
                error: function () {
                    console.log('error');
                }
            });
        });
    }
}

