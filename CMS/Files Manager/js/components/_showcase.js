﻿import { billetPrice } from './_functions.js'

export default function showcase(element = '.admake-showcase') {
    let _$el = $(element);

    if (_$el.length) {

        $('.ad-showcase-product').each(function () {
            let $flag = $(this).find('[class*="boleto"]'),
                $best = $(this).find('.best-price strong'),
                $discount = $(this).find('.percent.flag span'),
                $html = $(this).find('.best-price');

            if ($discount.length) {
                let value = parseFloat($discount.html().replace(' %', '').replace(',', '.'));
                $discount.text(value.toFixed() + ' %');
            }
            if( $flag.length ){
                billetPrice($flag, $best, $html, 0);
                $html.addClass('admake-has-best-price');
                $(this).find('.discount-msg').show();
                $(this).find('.best-price-not-discount').show();
            }


        });

        // MUTATION
        if ($('body#page-department').length) {
            let targetNode = document.querySelector('#department-showcase .admake-showcase');
            var callback = function () {
                $('.ad-showcase-product').each(function () {
                    let $flag = $(this).find('[class*="boleto"]'),
                        $best = $(this).find('.best-price strong'),
                        $discount = $(this).find('.percent.flag span'),
                        $html = $(this).find('.best-price');

                    if ($discount.length) {
                        let value = parseFloat($discount.html().replace(' %', '').replace(',', '.'));
                        $discount.text(value.toFixed() + ' %');
                    }
                    
                    if( $flag.length ){
                        billetPrice($flag, $best, $html, 0);
                        $html.addClass('admake-has-best-price');
                        $(this).find('.discount-msg').show();
                        $(this).find('.best-price-not-discount').show();
                    }
                });
            };
            // var observer = new MutationObserver(callback);
            // observer.observe(targetNode, { childList: true });
        }

    }
}
