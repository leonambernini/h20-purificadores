﻿export default function titleContentExists(element = '[data-admake="title-content-exits"]') {
    
    $.each( $(element), function(){
        let $this = $(this);
        let $title = $($this.attr('data-title'), $this); 
        let $content = $($this.attr('data-content'), $this); 

        if( $title.length && $content.length ){

            $title.hide();

            if( $content.html() !== undefined && $content.html() !== null && $.trim( $content.html() ) !== '' ){
                $title.show();
            }
        }
    });
    
}