﻿const setCookies = (key, value, option = 'local') => {
    if (typeof (Storage) !== "undefined") {
        switch (option) {
            case 'local':
                localStorage.setItem(key, value);
                break;

            case 'section':
                sessionStorage.setItem(key, value);
                break;

            default:
                break;
        }
    }
    return false;
}

const getCookies = (key, option = 'local') => {
    if (typeof (Storage) !== "undefined") {
        switch (option) {
            case 'local':
                return localStorage.getItem(key) === null ? false : localStorage.getItem(key);

            case 'section':
                return sessionStorage.getItem(key) === null ? false : sessionStorage.getItem(key);

            default:
                break;
        }
    }
    return false;
}

const stringToSlug = (str) => {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var f = "ãàáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var t = "aaaaaeeeeiiiioooouuuunc------";
    for (var i = 0, l = f.length; i < l; i++) {
        str = str.replace(new RegExp(f.charAt(i), 'g'), t.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}

const scrollHeader = (element = '#header') => {

    const $body = $('body');

    if ((window).outerWidth > 1199) {
        $body.css('padding-top', $(element).outerHeight());
    } else {
        $body.css('padding-top', 0);
    }
    $(window).scroll(function () {
        if ((window).outerWidth > 1199) {
            $body.css('padding-top', $(element).outerHeight());
        } else {
            $body.css('padding-top', 0);
        }
    }).resize(function () {
        if ((window).outerWidth > 1199) {
            $body.css('padding-top', $(element).outerHeight());
        } else {
            $body.css('padding-top', 0);
        }
    });
}

const reset = () => {
    // REMOVE BOOTSTRAP 2 CSS
    if ($('link[href*="/bootstrap/2"]').length && ($('body').hasClass('ad-orders'))) {
        $('link[href*="/bootstrap/2"]').remove();
    }
    // REMOVE BOOTSTRAP 2 CSS
}

const changeUserLogin = () => {
    if ($('.col-header-login a[href="/no-cache/user/logout"]').length && !$('.welcome .account').length) {
        $('#header p.welcome').show();
        $('.col-header-login a[href="/no-cache/user/logout"]').text('Sair');
        $('.col-header-login a[href="/no-cache/user/logout"]').addClass('logout').before('<a href="/_secure/account" class="account">Minha conta</a>');
    } else {
        $('#header p.welcome').html('Minha Conta <em><a id="login">Olá! Faça seu login</a></em>').show();
    }
}

const formatMoneyCheckout = (e, c, d, t) => {
    e = e.toString();
    if (e.indexOf('.') < 0 && e.indexOf(',') < 0) {
        e = e.substr(0, e.length - 2) + '.' + e.substr(-2);
    }
    var n = e,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

const formatMoney = (e, c, d, t) => {
    var n = e,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

const convertStringMoneytoFloat = function (text, currency = 'R$') {
    let result;

    result = text.replace(currency, '');
    result = result.replace('.', '');
    result = result.replace(',', '.');

    return parseFloat(result);
}

const billetPrice = function($flag = $('.product-image-flags .discount > p[class*="boleto"]'), $best = $('.skuBestPrice'), $html = $('.price-best-price'), product = true) {
    if($flag.length){
        let percent = parseInt($flag.text().split(' ')[0].replace('%', '')),
            bestPrice = convertStringMoneytoFloat($best.text()),
            newBestPrice = ((100 - percent) * bestPrice) / 100;

            // console.log($flag.text(),bestPrice, newBestPrice)

        // if (product) {
            $html.html(`<strong>R$ ${formatMoney(newBestPrice, 2, ',', '.')}</strong> à vista`);
        // }else{
        //     $html.html(`<strong>R$ ${formatMoney(newBestPrice, 2, ',', '.')}</strong> à vista`);
        // }
        $flag.hide();
    }
}

const getSubString = function (initial, final, str) {
    let sub = str.substring(str.lastIndexOf(initial), str.lastIndexOf(final));
    return sub != '' ? sub : false;
}

const getParameter = function (name, location) {
    var url;

    if (location) {
        location = location.replace('amp;', '');
        url = location;
    } else {
        url = window.location.search
    }

    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);

    return results == null ? null : results[1];
};

const checkSkuVariation = function (element = '#page-product a.buy-button') {
    var $skuSelector = $('.sku-selector-container');
    if ($(element).length) {
        if ($(element).attr('href').indexOf('javascript:alert') > -1) {
            $skuSelector.addClass('sku-selector-alert');
            return false;
        } else {
            $skuSelector.removeClass('sku-selector-alert');
            return true;
        }
    }
    return false;
}

const accordion = function (btn = '.accordion-btn', parent = '.accordion-content') {
    if ($(btn).length) {

        $(document).on('click', btn, function () {
            var $this = $(this);
            var $p = $this.parents(parent);

            if ($p.hasClass('active')) {
                $p.removeClass('active');
            } else {
                $p.addClass('active');
            }
            return false;
        });
    }
}

const serializeArrayToJson = function (arr) {
    var json = {};
    $.each(arr, function (i, v) {
        json[v.name] = v.value;
    });
    return json;
}

const observerMutation = function (target, functionCallback, type = 'childList') {
    let targetNode = document.querySelector(target);

    var callback = function () {
        functionCallback();
    };

    var observer = new MutationObserver(callback);

    switch (type) {
        case 'attributes':
            observer.observe(targetNode, { attributes: true });
            break;

        case 'childList':
            observer.observe(targetNode, { childList: true });
            break;

        default:
            break;
    }
}


export { billetPrice, convertStringMoneytoFloat, observerMutation, stringToSlug, changeUserLogin, formatMoneyCheckout, formatMoney, reset, getParameter, getSubString, checkSkuVariation, accordion, serializeArrayToJson, scrollHeader, setCookies, getCookies }
