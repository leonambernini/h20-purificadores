import ProductImage from '../components/_product-image.js'
import Shipping from '../components/_shipping.js'
import Reviews from '../components/_reviews.js'
import addRemQty from '../components/_addRemQty.js'
import titleContentExists from '../components/_titleContentExists.js'
import { billetPrice, stringToSlug, formatMoneyCheckout, formatMoney } from '../components/_functions.js'

function showHideQty() {
    if ($('.product-buy .notifyme-form').length && $('.product-buy .notifyme-form').is(':visible')) {
        $('.product-buy').addClass('no-stock');
    } else {
        $('.product-buy').removeClass('no-stock');
    }
}

export default (element = '#page-product') => {

    if ($(element).length) {
        addRemQty();
        titleContentExists();
        showHideQty();

        new ProductImage;
        new Shipping;
        new Reviews;

        if( $('.seller-message .seller-name a').length ){

            var setImage = function(url){
                // $('.seller-message').addClass('d-flex justify-content-between align-items-center').append('<div class="seller-img"><b>Loja</b> <img src="'+url+'" class="img-seller img-fluid" /></div>');
            }

            $('.seller-name a').attr('href', '#').css('cursor', 'default');

            var $sellerLink = $('.seller-message .seller-name a');
            var sellerName = stringToSlug($sellerLink.text());
            var sellerId = 0, sellerImage = '/arquivos/seller-'+sellerName+'.png';
            for( var x = 0; x < dataLayer.length; x++ ){
                if( dataLayer[x]['sellerId'] != undefined && dataLayer[x]['sellerId'] != null && dataLayer[x]['sellerId'] != '' ){
                    sellerId = dataLayer[x]['sellerId'];
                }
            }

            if( sellerId != 0 ){
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "/api/catalog_system/pvt/seller/"+sellerId,
                    "method": "GET",
                    "headers": {
                      "Content-Type": "application/json",
                      "X-VTEX-API-AppKey": "vtexappkey-hdoiso-GLWSFJ",
                      "X-VTEX-API-AppToken": "LZKLUAGSUNKHAVLSISVRARQEQBXKGKTEGZYZNAOWKIJGSYDBSVTQOJJTJGUTDVMKTFBBXMEHEYHSHKMRYWSLQCPXQEPHGVSOWZNMXBDFRMMPBQMZUCVGDATQQDKSWWAI",
                      "Accept": "*/*",
                    }
                  }
                  
                  $.ajax(settings).done(function (response) {
                    if( response['UrlLogo'] != undefined && response['UrlLogo'] != null && response['UrlLogo'] != '' ){
                        setImage(response['UrlLogo']);
                    }else{
                        setImage(sellerImage);
                    }
                  });
            }else{
                setImage(sellerImage);
            }

        }

        if( $('.sellerOptions .sellers > h3').length ){
            $('.sellerOptions .sellers > h3').html('Ofertas de outros vendedores na H2O');
        }

        $.each( $('.sellerOptions ul li'), function(){
            var $this = $(this);
            var $link = $this.find('.buy-button-sellers');
            var id = $this.find('h4 > a').attr('href').replace('/seller-info?sellerId=', '');
            if( !$this.find('.seller-shipping').length ){
                $link.before('<div class="seller-shipping" data-seller-id="'+id+'"><b>A calcular</b><span>informe o CEP para valor do frete</span></div>');
            }
        });

        $('#cep-shipping').blur( function(){
            var $this = $(this);
            var cep = $(this).val();
            if( cep != undefined && cep != null && cep != '' && cep.length == 9 ){
                $.each( $('.seller-shipping'), function(){
                    var items = [{
                        id: vtxctx['skus'],
                        quantity: parseInt($('.quantitySelector input').val()),
                        seller: $(this).attr('data-seller-id')
                    }];
                    var postalCode = cep;
                    var country = 'BRA';

                    $(this).html('<i class="fas fa-circle-notch fa-spin"></i>');
                    vtexjs.checkout.simulateShipping(items, postalCode, country)
                        .done(function(result) {
                            console.log(result)
                            if( result['items'] != undefined && result['items'] != null && result['items'].length ){
                                var item = result['items'][0];
                                var logisticsInfo = result['logisticsInfo'][0]['slas'][0];
                                var $seller = $('.seller-shipping[data-seller-id="'+item['seller']+'"]');
                                if( $seller.length ){
                                    let days = parseInt(logisticsInfo.shippingEstimate);
                                    let textDays = days + ( ( days > 1 ) ? ' dias úteis' : ' dia útil' );
                                    $seller.html(
                                        '<table>'+
                                        '   <tr>' +
                                        '       <th>Entrega</th>' +
                                        '       <td>'+logisticsInfo['name']+'</td>' +
                                        '   </tr>' +
                                        '   <tr>' +
                                        '       <th>Frete</th>' +
                                        '       <td>R$ '+formatMoneyCheckout(logisticsInfo['price'], 2, ',', '.')+'</td>' +
                                        '   </tr>' +
                                        '   <tr>' +
                                        '       <th>Prazo</th>' +
                                        '       <td>'+textDays+'</td>' +
                                        '   </tr>' +
                                        '</table>'
                                    );
                                }
                            }
                        });
                });
            }
        });


        if( $('.other-payments .titulo-parcelamento').length ){
            $('.other-payments .titulo-parcelamento').html('Clique para opções de Parcelamento no Cartão de Crédito <i class="fas fa-angle-down"></i>');
        }
        $('.other-payments .titulo-parcelamento').click( function(){
            $('.other-payment-method-ul').toggleClass('active');
            return false;
        });

        $('.admake-anchor-btn').click( function(){
            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top - ( $('#buy-box-float').outerHeight() + 100 )
            }, 2000);
            return false;
        });

        if( $('.product-brand .brandName .brand').length ){
            let brandName = $('.product-brand .brandName .brand').text();
            let brandNameSlug = stringToSlug(brandName);
            let image_url = `https://hdoiso.myvtex.com/arquivos/brand-${brandNameSlug}.png`;
            $.get(image_url).done(function(){
                $('.product-brand .brandName .brand').html(`<img src="${image_url}" alt="${brandName}" class="img-fluid" />`);
            }).fail(function(){
                console.log(`Image brand ${brandName} 404: ${image_url}`);
            });
        }

        $('#buy-box-float .col-photo').html(`<img src="${$('#image-main').attr('src')}" alt="" width="55" class="img-fluid" />`);
        $('#buy-box-float .col-name').html(`<p>${$('.product-name > .productName').html()}<p/>`);
        $('#buy-box-float .col-price').html($('.product-price-variation .product-price').html());
        
        $(window).scroll( function(){
            if( $('.price-border .no-stock').length && $('#buy-box-float').is(':visble') ){
                $('#buy-box-float').fadeOut();
            }else if( !$('.price-border .no-stock').length ){
                if( $(this).scrollTop() >= $('.product-price-variation').offset().top && !$('#buy-box-float').is(':visible') ) {
                    $('#buy-box-float').fadeIn();
                }else if( $(this).scrollTop() < $('.product-price-variation').offset().top && $('#buy-box-float').is(':visible') ) {
                    $('#buy-box-float').fadeOut();
                }
            }
            // if( $(this).width() < 768 && !$('#header').hasClass('fixed-top') ){
            //     $('#buy-box-float').removeClass('fixed-top sticky-top').fadeOut();
            //     $('#header').addClass('fixed-top sticky-top');
            // }else if( $(this).width() >= 768 ){
            //     if( $(this).scrollTop() >= $('.product-price-variation').offset().top && !$('#buy-box-float').is(':visible') ) {
            //         $('#buy-box-float').addClass('fixed-top sticky-top').fadeIn();
            //         $('#header').removeClass('fixed-top sticky-top');        
            //     }else if( $(this).scrollTop() < $('.product-price-variation').offset().top && $('#buy-box-float').is(':visible') ) {
            //         $('#buy-box-float').removeClass('fixed-top sticky-top').fadeOut();
            //         $('#header').addClass('fixed-top sticky-top');        
            //     }
            // }
        });


        if( $('#especificacoes-anchor').length ){
            let $especificacoes = $('#especificacoes-anchor .content');
            if( $('#js-help-description-product table.group.Especificacoes-Tecnicas').length ){
                let $table = $('#js-help-description-product table.group.Especificacoes-Tecnicas');
                
                $especificacoes.html($table.clone());
            }else{
                $('#especificacoes-anchor .content, .admake-anchor-btn[href="#especificacoes-anchor"]').hide();
            }

            if( $('#js-help-description-product table.group.Manual').length ){
                $especificacoes.append($('#js-help-description-product table.group.Manual .value-field').html());
                $('#especificacoes-anchor .content, .admake-anchor-btn[href="#especificacoes-anchor"]').show();
            }
        }
        // if( $('#diferenciais-anchor').length ){
        //     let $diferenciais = $('#diferenciais-anchor .content');
        //     if( $('#js-help-description-product table.group.Diferenciais').length ){
        //         let $table = $('#js-help-description-product table.group.Diferenciais');
        //         let html = '';

        //         $.each( $table.find('tr'), function(){
        //             let $value = $(this).find('.value-field');

        //             html += $value.html();
        //         });

        //         $diferenciais.html('<ul class="d-flex justify-content-around align-items-stretch flex-wrap">' + html + '</ul>');
        //     }else{
        //         $('.admake-anchor-btn[href="#diferenciais-anchor"]').parents('li').remove();
        //         $('#diferenciais-anchor').remove();
        //     }
        // }
        if( $('#diferenciais-anchor').length ){
            let $diferenciais = $('#diferenciais-anchor .content');
            if( $('#js-help-description-product table.group.Diferenciais').length ){
                let $table = $('#js-help-description-product table.group.Diferenciais');
                let html = '';

                $.each( $table.find('.value-field[class*="titulo"]'), function(){
                    let $valueTitle = $(this);
                    let $tr = $valueTitle.parents('tr');
                    let $valueText = $('+ tr', $tr).find('.value-field[class*="texto"]');

                    let _html = `<li><strong>${$valueTitle.html()}</strong> <span>${$valueText.html()}</span></li>`;

                    html += _html;
                });

                $diferenciais.html('<ul class="d-flex justify-content-around align-items-stretch flex-wrap">' + html + '</ul>');
            }else{
                $('.admake-anchor-btn[href="#diferenciais-anchor"]').parents('li').remove();
                $('#diferenciais-anchor').remove();
            }
        }
        if( $('#caracteristicas-anchor').length ){
            let $caracteristicas = $('#caracteristicas-anchor .content');
            if( $('#js-help-description-product table.group.Caracteristicas').length ){
                let $table = $('#js-help-description-product table.group.Caracteristicas');
                let html = '';

                $.each( $table.find('tr'), function(){
                    let $name = $(this).find('.name-field');
                    let $value = $(this).find('.value-field');

                    html += '<li class="caracteristicas-item"><div class="caracteristica-border"><strong>' + $name.html() + '</strong>' + $value.html() + '</div></li>';
                });

                $caracteristicas.html('<ul class="d-flex justify-content-begin align-items-stretch flex-wrap">' + html + '</ul>');
            }else{
                $('.admake-anchor-btn[href="#caracteristicas-anchor"]').parents('li').remove();
                $('#caracteristicas-anchor').remove();
            }
        }
        if( $('#descricao-anchor').length ){
            let $descricao = $('#descricao-anchor .content');
            let htmlCompatibilidade = '', htmlInformacoesAdicionais = '', htmlBanner = '', htmlCol = '{description}';
            if( $('#js-help-description-product table.group.Descricao').length ){
                let $table = $('#js-help-description-product table.group.Descricao');

                if( $table.find('.value-field.banner').length ){
                    htmlCol = '<div class="col-12 col-sm-7">{description}</div>';
                    htmlBanner = '<div class="col-12 col-sm-5">' + $table.find('.value-field.banner').html() + '</div>';
                }

                if( $table.find('.value-field.Compatibilidade').length ){
                    htmlCompatibilidade = '<div class="compatibilidade pb-5"><h3>Compatibilidade</h3><p>' + $table.find('.value-field.Compatibilidade').html() + '</p></div>';
                }

                if( $table.find('.value-field.informacoes-adicionais').length ){
                    htmlInformacoesAdicionais = '<div class="informacoes-adicionais pt-5"><h3>Informações adicionais</h3><p>' + $table.find('.value-field.informacoes-adicionais').html() + '</p></div>';
                }
            }
            $descricao.html(`<div class="description-box">
                                <div class="row">
                                    ${htmlBanner}
                                    ${ htmlCol.replace('{description}', htmlCompatibilidade + '<div class="descricao"><h3>Descrição</h3>' + $('#js-help-description-product .productDescription').html() + '</div>' + htmlInformacoesAdicionais)}
                                </div>
                            </div>`);
            // if( $('#js-help-description-product table.group.Descricao').length ){
            //     let $table = $('#js-help-description-product table.group.Descricao');
            //     let html = '', htmlBanner = '';

            //     if( $table.find('.value-field.banner').length ){
            //         htmlBanner = '<div class="col-12 col-sm-5">' + $table.find('.value-field.banner').html() + '</div>';
            //     }
            //     if( $table.find('.value-field.Texto-Destaque').length ){
            //         html += '<div class="description-box">' + $table.find('.value-field.Texto-Destaque').html();
            //     }
            //     if( $table.find('.value-field.informacoes-adicionais').length ){
            //         html += '<h4>Informações adicionais</h4>' + $table.find('.value-field.informacoes-adicionais').html();
            //     }
            //     html += '</div>';

            //     if( htmlBanner != '' ){
            //         html = '<div class="row align-items-center">' + htmlBanner + '<div class="col-12 col-sm-7">' + html + '</div></div>';
            //     }

            //     $descricao.html(html);
            // }else if( $('#js-help-description-product .productDescription').length ){
                
            // }else{
            //     $('.admake-anchor-btn[href="#descricao-anchor"]').parents('li').remove();
            //     $('#descricao-anchor').remove();
            // }
        }
        if( $('#admake-product-js-infos').length ){
            let $box = $('#admake-product-js-infos');
            if( $('#js-help-description-product table.group.informacoes-verdes').length ){
                let $table = $('#js-help-description-product table.group.informacoes-verdes');
                let html = '';

                $.each( $table.find('tr'), function(){
                    let $name = $(this).find('.name-field');
                    let $value = $(this).find('.value-field');

                    html += '<li class="informacoes-item"><div class="informacoes-border"><strong>' + $name.html() + '</strong>' + $value.html() + '</div></li>';
                });

                $box.append('<ul class="info-verde d-flex justify-content-around align-items-stretch flex-wrap">' + html + '</ul>');
            }
            if( $('#js-help-description-product table.group.informacoes-azuis').length ){
                let $table = $('#js-help-description-product table.group.informacoes-azuis');
                let html = '';

                $.each( $table.find('tr'), function(){
                    let $name = $(this).find('.name-field');
                    let $value = $(this).find('.value-field');

                    html += '<li class="informacoes-item"><div class="informacoes-border"><strong>' + $name.html() + '</strong>' + $value.html() + '</div></li>';
                });

                $box.append('<ul class="info-azul d-flex justify-content-around align-items-stretch flex-wrap">' + html + '</ul>');
            }
        }

        if( $('.value-field.Modelo').length && $('.productReference').length ){
            $('.productReference').html($('.value-field.Modelo').html());
        }

        //Video-do-Produto
        // $('#js-help-description-product').remove();

        if( $('#divCompreJunto table').length && $('.admake-buy-together').length ){
            $('.admake-buy-together').fadeIn();
        }

        // let ajustShippingBox = function(){
        //     console.log( $(window).width() )
        //     if( $(window).width() >= 768 && !$('#product-shipping-desktop .ad-shipping-form').length ){
        //         $('#product-shipping-desktop').html( $('.product-shipping') );
        //     }else if( $(window).width() < 768 && !$('.product-shipping-mobile .ad-shipping-form').length ){
        //         $('.product-shipping-mobile').html( $('.product-shipping') );
        //     }
        // }
        // ajustShippingBox();
        // $(document).resize( function(){
        //     ajustShippingBox();
        // })

        if( $('.valor-por.price-best-price').length ){
            $.each( $('.valor-por.price-best-price'), function(){
                $(this).after('<div class="msg-a-vista">1X no cartão ou boleto</div>');
            });
        }

        if( $('.valor-dividido.price-installments').length && $('.other-payment-method-ul > li').length ){
            var $item = $('.other-payment-method-ul > li').last();
            var qtd = parseInt($item.find('span').eq(0).text());
            $.each( $('.valor-dividido.price-installments'), function(){
                if( $(this).text().indexOf('1x') >= 0 ){
                    var valorParcelado = $item.find('strong').text();
                    $(this).html($(this).html().replace('ou', 'ou até'));
                    $(this).find('.skuBestInstallmentNumber').html(qtd.toString() + '<span class="x">x</span>');
                    $(this).find('.skuBestInstallmentValue').html(valorParcelado);
                }
                valorParcelado = parseFloat(valorParcelado.replace('R$ ', '').replace('.', '').replace(',', '.'));
                console.log(qtd, valorParcelado, qtd * valorParcelado)
                $(this).after('<span class="valor-total-parcelado">Total parcelado: R$ '+formatMoney(qtd * valorParcelado, 2, ',', '.')+'</span>')
                $(this).show();
            });
        }

        let discountBoleto = function(){
            if( $('.col-image .discount').length ) {
                let $flag = $('.col-image .discount').find('[class*="boleto"]'),
                    $best = $('.descricao-preco .skuBestPrice'),
                    $html = $('.plugin-preco .preco-a-vista');

                $('.descricao-preco .valor-dividido.price-installments').before('<div class="product-best-price-discount"></div><div class="best-price-msg">desconto à vista 1X cartão ou boleto</div>');
                $('.descricao-preco').after('<div class="new-sku-best-price"><span>Total a prazo </span>' + $best.html() + '</div>');

                $('.productPrice .preco-a-vista.price-cash').hide();
                $best.hide();
                billetPrice($flag, $best, $('.product-best-price-discount'), 1);

                $('.productPrice').addClass('active');
            };
        }
        // discountBoleto();

        // $('.sku-selector').change( function(){
        //     setTimeout( function(){
                
        //     }, 500);
        // });

        if( $('.plugin-preco .economia-de').length ){
            $('.plugin-preco .economia-de').html( $('.plugin-preco .economia-de').html().replace('Economia de', 'Compre e economize'))
        }

        if ($('.quantitySelector > label > input').length) {
            $('.quantitySelector > label').append(`
                    <div class="actions">
                        <button class="add-qty btn btn-add-remove" parent="#ad-product-buy-button" element=".quantitySelector > label > input" type="add" tabindex="0">+</button>
                        <button class="remove-qty btn btn-add-remove" parent="#ad-product-buy-button" element=".quantitySelector > label > input" type="remove" tabindex="0">-</button>
                    </div>`);
        }

        // events
        $(document).on('change', '.sku-selector', function () {
            showHideQty();
            // setTimeout( function(){
                discountBoleto();
            // }, 500);
        });

        // if( $('.thumbs').length ){
            
            $('.zoomWrapper').click( function(){

                if( !$('#modal-photos').length ){
                    let  htmlPhotos = '';
                    $.each( $('.thumbs a'), function(){
                        let $this = $(this);
                        let image = $this.attr('rel');
                        
                        htmlPhotos += `<div class="item"><img src="${image}" alt="" /></div>`;
                    });

                    $('body').append(`
                        <div id="modal-photos">
                            <div class="mask"></div>
                            <div class="content">
                                <div class="modal-photos-carousel">
                                    ${htmlPhotos}
                                </div>
                            </div>
                        </div>
                    `);
                    setTimeout( function(){
                        $('#modal-photos .modal-photos-carousel').slick({
                            nextArrow: '<button type="button" class="slick-next btn"><i class="fas fa-angle-right"></i></button>',
                            prevArrow: '<button type="button" class="slick-prev btn"><i class="fas fa-angle-left"></i></button>',
                        });
                    }, 300)
                    $('#modal-photos .mask').click( function(){
                        $('#modal-photos').fadeOut();
                        return false;
                    });
                }

                $('#modal-photos').fadeIn();

                return false;
            });
        // }
    }
}
