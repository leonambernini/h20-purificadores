﻿import getVariables from '../components/_variables.js'
const _variables = getVariables();

export default (element = '#page-home') => {
    if ($(element).length) {

        $.each( $('#mini-banner-02 a'), function(){
            let $this = $(this);
            if( $this.attr('href').indexOf('http') >= 0 ){
                $this.attr('target', '_blank');
            }
        });

        $.each( $('.showcase-home'), function(){
            var $this = $(this);
            var $title = $this.find('.showcase-title .title-text');
            var $admakeShowcase = $this.find('.admake-showcase > h2');

            if( $title.length && $admakeShowcase.length ){
                $title.append($admakeShowcase);
            }
        });

        if ($(window).outerWidth() < 768) {
            let finalConfig = {
                ..._variables.carousel, ...{
                    slidesToShow: 3,
                    autoplay: true,
                    autoplaySpeed: 2000,
                    responsive: [
                        {
                            breakpoint: 576,
                            settings: {
                                slidesToShow: 2,
                                variableWidth: false
                            }
                        },
                    ]
                }
            };

            $('#home-brands .content').slick(finalConfig);
        }

        function filtroHome(){
            $('#select-brand, #select-model').attr('disabled', 'disabled');

            let _brands = ['3M', 'Consul', 'Electrolux', 'IBBL', 'Latina', 'Lorenzetti', 'Planeta Água', 'Polar', 'Soft', 'New Up', 'Colormaq', 'Esmaltec', 'Europa', 'Purific', 'Hoken', 'Libell', 'Lider', 'Master Frio'];

            // $.ajax({
            //     url: "/api/catalog_system/pvt/brand/list",
            //     type: "GET",
            //     headers: {
            //         "Content-Type": "application/json",
            //         "X-VTEX-API-AppKey": "vtexappkey-hdoiso-KIOAIF",
            //         "X-VTEX-API-AppToken": "ZCERJZYLDXHSQRQMNLINFXQOTGMIITIJWYGAAVPPUHPQMWFXUBNOHVXCLQYCMPCJPXWSKQMRWTTXIABXAONHGTCAAEOKXMCEEGBOANNEPVROCZULOBCYCVCVVVRFBBUK",
            //     },
            // }).done(function(response){
            //     for(var i=0;i<response.length; i++ ){
            //         if(response[i].isActive==true && _brands.indexOf(response[i].name) >= 0 ){
                        for( var i=0;i<_brands.length;i++){
                            $('#select-brand').append('<option value="' +_brands[i]+ '" data-brand-id="'+_brands[i]+'">' +_brands[i]+ '</option>')
                        }
                //     }
                // }
                $('#select-brand').removeAttr('disabled');
            // });
        }
        $('#select-brand').off('change')
        $('#select-brand').on('change', function(){
            // var brandId=$(this).find('option:selected').attr('data-brand-id');
            var brandId=$(this).find('option:selected').val();
            $.ajax({
                // url: "/api/catalog_system/pub/products/search?fq=C:2&fq=B:"+brandId,
                url: "/api/catalog_system/pub/products/search?fq=C:1&fq=specificationFilter_139:"+brandId,
                // url: "/api/catalog_system/pub/specification/fieldvalue/138?fq=B:"+brandId,
                type: "GET"
             }).done(function(response){

                console.log(response)
                var obj={}
                var htmlFinal = '<option value="" disabled selected>Selecione o Modelo do Purificador</option>'
                for(var i=0;i<response.length; i++ ){
                    var excludeCategory = false;
                    for(var j=0; j<response[i].categoriesIds; j++){
                        if(response[i].categoriesIds[j]=='/1/'){
                            excludeCategory = true;
                        }
                    }
                    if(excludeCategory == true){
                        return true;
                    }else{
                        
                        if(typeof response[i]['Modelo do Purificador'] != 'undefined'){
                            for( let x=0; x<response[i]['Modelo do Purificador'].length; x++){
                                obj[response[i]['Modelo do Purificador'][x]] = response[i]['Modelo do Purificador'][x];
                            }
                            // htmlFinal += '<option value="' + response[i].Modelo + '" >' + response[i].Modelo + '</option>'
                        }
                    }
                }
                for(let key in obj){
                    htmlFinal += '<option value="' + key + '" >' + key + '</option>'
                }
                $('#select-model').html(htmlFinal);
                $('#select-model').removeAttr('disabled');
                $('#select-model').parents('.options-box').addClass('active');
             });
        });
        $(document).on('click', '.admake-filter-home .btn-success', function(){
            var model = $('#select-model option:selected').val();
            // var brand  = $('#select-brand option:selected').attr('data-brand-id');
            var brand  = $('#select-brand option:selected').val();
            window.location.href = ('href',`/refil-filtro-de-reposicao/${brand}/${model}?PS=16&map=c,specificationFilter_139,specificationFilter_138`);
            // console.log('href',`/refil-filtro-de-reposicao/${brand}/${model}?PS=16&map=c,specificationFilter_139,specificationFilter_138`);
            // window.location.href = ('href',`/refil-filtro-de-reposicao/${brand}/${model}?PS=16&map=c,b,specificationFilter_138`);
        });

        filtroHome();

    }
}