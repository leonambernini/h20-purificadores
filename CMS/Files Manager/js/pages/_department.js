import ShowcaseDepartment from '../components/_showcaseDepartment.js'
// import PriceRange from '../components/_priceRange.js'

function getQueryParams(qs) {
    qs = qs.split('+').join(' ');

    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }

    return params;
}

function checkImage(imageSrc, good, bad) {
    var img = new Image();
    img.onload = good; 
    img.onerror = bad;
    img.src = imageSrc;
}

export default (element = '#page-department') => {
    if ($(element).length) {

        // new PriceRange;
        new ShowcaseDepartment;

        $(document).on('click', '.pages > li', function(){
            // console.log('asda')
            $([document.documentElement, document.body]).animate({
                scrollTop: 0
            }, 500);
        });

        if( $('#banner-by-params').length ){
            let query = getQueryParams(document.location.search);
            if( query.fq != undefined && query.fq != null ){
                let queryParam = query.fq.split(':');
                queryParam = (queryParam[1]).toLowerCase();
                let image = "/arquivos/banner-busca-" + queryParam + '.png';
                checkImage(image, function(){ $('#banner-by-params .container').html('<img src="'+image+'" alt="'+ queryParam +'" class="img-fluid w-100"/>') }, function(){ console.log("bad"); } );

            }
        }

        /* filter*/
        $('.resultado-busca-filtro').prepend(`<button id="open-filter-mobile" class="btn d-block d-sm-none"><i class="fas fa-filter"></i>Filtrar</button>`);
        $('.col-navigator').prepend(`<button id="close-filter-mobile" class="btn d-block d-sm-none"><i class="fas fa-filter"></i>Filtrar por <i class="fas fa-times"></i></button>`);
        $('#open-filter-mobile, #close-filter-mobile').click( function(){
            $('.col-navigator').toggleClass('active-mobile');
            return false;
        });
        $('#department-showcase .sub').prepend($('#department-showcase .resultado-busca-numero').eq(0));
        // $('.search-single-navigator').prepend('<div class="navigator-header"></div>');
        // $('.search-single-navigator h5').eq(0).before('<h5 class="title">Filtrar por</h5>');

        $('.search-single-navigator > h5, .search-single-navigator > h3').click(function () {
            var $this = $(this);
            $this.toggleClass('hide');
            return false;
        });
        $('#department-navigator h3').each(function () {
            let $this = $(this),
                $ul = $this.find('+ul');

            $('.search-single-navigator .navigator-header').append($this);
            if ($ul.find('>li').length) {
                $('.search-single-navigator .navigator-header').append($ul);
            } else {
                $ul.remove();
            }
        });

        $('#department-navigator h4 + ul').each(function () {
            let $this = $(this);
            if (!$this.find('>li').length) {
                $this.remove();
            }
        });
        // $('#department-navigator h5:not(.title)').before('<hr>');

        //events
        $(document).on('click', '#open-filter-mobile', function () {
            $(`#page-department .col-navigator`).fadeIn(100, function () {
                $(`.col-navigator .content`).animate({
                    left: 0
                }, 300);
            });
        });

        $(document).on('click', '.close-filter-mobile, .col-navigator .mask', function () {
            $(`.col-navigator .content`).animate({
                left: '-100%'
            }, 300, function () {
                $(`#page-department .col-navigator`).fadeOut(100);
            });
        });
    }
}