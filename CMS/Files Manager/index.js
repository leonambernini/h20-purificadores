/*************** CSS ***************/
import './css/sass/admake-default.scss'
// import './css/sass/admake-checkout.scss'

/*************** JS ***************/

// PLUGINS
import './js/plugins/slick.js'
import './js/plugins/jquery.mask.js'
import './js/plugins/QD_infinityScroll.js'

// FUCTIONS
import { observerMutation, reset, accordion } from './js/components/_functions.js'

// COMPONENTS
import carroselDefault from './js/components/_carrosel.js'
import Cart from './js/components/_cart.js'
import Navbar from './js/components/_navbar.js'
import Newsletter from './js/components/_newsletter.js'
import showcase from './js/components/_showcase'

// PAGES
import Institucional from './js/pages/_institucional.js'
import Department from './js/pages/_department.js'
import Product from './js/pages/_product.js'
import Account from './js/pages/_account.js'
import Home from './js/pages/_home.js'
// import CreateYourShop from './js/pages/_create-your-shop.js'

function init() {
    observerMutation('.ui-autocomplete', function () {
        $('.ui-autocomplete li img').each(function () {
            var $this = $(this), href = $this.attr('src').replace('25-25', '90-90');
            $this.attr('src', href);
        });
    });

    $.ajax({
        url: '/no-cache/profileSystem/getProfile',
        type: 'GET',
        success: function (dataProfile) {
            if (dataProfile.IsUserDefined) {
                $('.col-header-login').addClass('logged');
                if (dataProfile.FirstName) {
                    $('.col-header-login button > span').text(dataProfile.FirstName);
                } else {
                    $('.col-header-login button > span').text(dataProfile.Email);
                }
            } else {
                $('.col-header-login').removeClass('logged');
            }
        },
        error: function () {
            callback.erroAjaxcallback('construtor');
        },
        complete: function () {
            $('.col-header-login .content').show();
        }
    });

    $('#btn-open-search').on('click', function () {
        $('#header-main > .container > .row').toggleClass('active');
    });
}

$(document).ready(function () {

    console.log('Correção CORS - Admake');

    reset();
    init();
    carroselDefault();
    accordion();
    showcase();


    /** PAGES */
    Home();
    Institucional();
    Department();
    Product();
    Account();
    /** PAGES */

    new Navbar;
    new Cart;
    new Newsletter;

    let bannerIsOpen = localStorage.getItem('admake-banner-popup') || false;

    if( bannerIsOpen == false ){
        localStorage.setItem('admake-banner-popup', true);
        if( $('.admake-banner-popup img').length ){
            $('.admake-banner-popup').fadeIn();
            $('.admake-banner-popup .mask').click( function(){
                $('.admake-banner-popup').fadeOut();
                return false;
            });
        }
    }


    vtexjs.checkout.getOrderForm().done(function(orderForm) {
        if( orderForm.loggedIn ){
            $('.link-my-account [href="/account"] > span').html('Minha Conta');
        }
    });

    let $qtyCart = $('.link-item-cart .badge');
    if( $qtyCart.length ){
        vtexjs.checkout.getOrderForm().done(function(orderForm) {
            let total = 0;
            for( let x = 0; x < orderForm.items.length; x++ ){
                let item = orderForm.items[x];
                total += item['quantity'];
            }

            $qtyCart.html(total);
        });
    }

    // $.each( $('#header-nav .sub-banner'), function(){
    //     let $this = $(this);
    //     let $parents = $this.parents('.all-departments-sub');
    //     let $box = $parents.find('.sub-items');
    //     if( $this.find('img').length ){
    //         let $image = $this.find('img');
    //         let $newImage = $image.clone();
    //         $box.append( $newImage );
    //     }
    //     $this.remove();
    // });

    // $('#header-nav .header-nav-item-all .header-nav-sub-item').mouseover( function(){
    //     let $this = $(this);
    //     let $banner = $this.find('.sub-banner img');
    //     let $box = $this.parents('.header-nav-sub-box');
    //     $box.find('>img').remove();
    //     if( $banner.length ){
    //         $box.append($banner.clone())
    //     }
    //     return false;
    // });

    if( $('.col-benefits').length ){
        $('.col-benefits > ul').slick({
            dots: false,
            arrows: false,
            autoplay: true,
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [{
                    breakpoint: 1199,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },{
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },{
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }


    if( $('.get-store-address-list').length && $('#address-store-aux').length ){
        $('.get-store-address-list').html($('#address-store-aux').html());
    }

    $('.adamke-tabs-list .admake-tab-btn').click( function(){
        var $this = $(this);
        var $btnList = $this.parents('.adamke-tabs-list');
        var $btns = $btnList.find('.admake-tab-btn');
        var id = $this.attr('href');
        if( $(id).length ){
            $btns.removeClass('active');
            $this.addClass('active');

            var $element = $(id);
            var $groupElements = $element.parents('.adamke-tabs-content-box');
            var $contents = $groupElements.find('.adamke-tabs-content');

            $contents.hide();
            $element.slideDown();
            return false;
        }
    });
    $.each( $('.adamke-tabs-list'), function(){
        var $btn = $(this).find('.admake-tab-btn').eq(0);

        if( $btn.length ){
            $btn.click();
        }
    });
});